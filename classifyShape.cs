using System.IO.Compression;
using System.Net.NetworkInformation;

namespace Milestone;
using System.Drawing;
using System.Drawing.Imaging;


public class classifyShape
{
    public static bool checkGrayScaleOrNot(Bitmap bmp)
    {
        if (bmp.PixelFormat == PixelFormat.Format8bppIndexed)
        {
            return true;
        }

        return false;

    }
    
    public static int Mean(Bitmap image)
    {
        int sum = 0;
        int totalNumber = image.Width * image.Height;

        unsafe
        {
            int height = image.Height;
            int width = image.Width;
            BitmapData imageData = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);
            byte* scan0 = (byte*)imageData.Scan0.ToPointer();
            int stride = imageData.Stride;

            for (int y = 0; y < height; y++)
            {
                byte* row = scan0 + (y * stride);

                for (int x = 0; x < width; x++)
                {
                    byte pixelValue = row[x];
                    sum += pixelValue;
                    
                }
            }

            image.UnlockBits(imageData);
        }

        int mean = sum / totalNumber;
        return mean;
    }


    public static Bitmap convertBinaryImage8bits(Bitmap grayImage, int threshold)
    {

        

        Rectangle rect = new Rectangle(0, 0, grayImage.Width, grayImage.Height);
        BitmapData grayData = grayImage.LockBits(rect, ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);
        int stride = grayData.Stride;
        IntPtr pixelData = grayData.Scan0;

        unsafe
        {
            byte * pGray = (byte *)(void *)pixelData;
            int nHeight = grayImage.Height;
            int nWidth = grayImage.Width;
            for (int y = 0; y < nHeight; y++)
            {
                for (int x = 0; x < nWidth; x++)
                {

                    byte grayValue = pGray[y * stride + x];

                    if (grayValue >= threshold)
                    {
                        pGray[y * stride + x] = 0;
                    }
                    else
                    {
                        pGray[y * stride + x] = 255;
                    }
                }
            }
        }

        grayImage.UnlockBits(grayData);
        
        return grayImage;
    }


    public static Bitmap LabelConnectedComponents(Bitmap inputImage)
    {
        Bitmap outputImage = new Bitmap(inputImage.Width, inputImage.Height, PixelFormat.Format8bppIndexed);


        BitmapData inputData = inputImage.LockBits(new Rectangle(0, 0, inputImage.Width, inputImage.Height),
            ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);
        BitmapData outputData = outputImage.LockBits(new Rectangle(0, 0, outputImage.Width, outputImage.Height),
            ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);


        int inputStride = inputData.Stride;
        int outputStride = outputData.Stride;


        IntPtr inputScan0 = inputData.Scan0;
        IntPtr outputScan0 = outputData.Scan0;

        int height = inputImage.Height;
        int width = inputImage.Width;
        int[] labelTable = new int[width * height];

        unsafe
        {
            byte * inputPtr = (byte *)(void *)inputScan0;
            byte * outputPtr = (byte *)(void *)outputScan0;

            int currentLabel = 0;

            
            for (int y = 0; y < height; y++)
            {
                byte * inputRow = inputPtr + y * inputStride;
                byte * outputRow = outputPtr + y * outputStride;

                for (int x = 0; x < width; x++)
                {
                    byte pixelValue = inputRow[x];

                    if (pixelValue != 0)
                    {
                        int label = 0;

                         if (x > 0 && inputRow[x - 1] != 0)
                            label = labelTable[y * width + x - 1];
                        else if (y > 0 && inputPtr[(y - 1) * inputStride + x] != 0)
                            label = labelTable[(y - 1) * width + x];
                        else if (x > 0 && y > 0 && inputPtr[(y - 1) * inputStride + x - 1] != 0)
                            label = labelTable[(y - 1) * width + x - 1];
                        else if (x < width - 1 && y > 0 && inputPtr[(y - 1) * inputStride + x + 1] != 0)
                            label = labelTable[(y - 1) * width + x + 1];
                        else if (x > 0 && y < height - 1 && inputPtr[(y + 1) * inputStride + x - 1] != 0)
                            label = labelTable[(y + 1) * width + x - 1];
                        else if (y < height - 1 && inputPtr[(y + 1) * inputStride + x] != 0)
                            label = labelTable[(y + 1) * width + x];
                        else if (x < width - 1 && y < height - 1 && inputPtr[(y + 1) * inputStride + x + 1] != 0)
                            label = labelTable[(y + 1) * width + x + 1];

                        
                        if (label == 0)
                            label = ++currentLabel;

                        labelTable[y * width + x] = label;
                        outputRow[x] = (byte)label;
                    }
                }
            }

          
            for (int y = 0; y < height; y++)
            {
                byte * outputRow = outputPtr + y * outputStride;

                for (int x = 0; x < width; x++)
                {
                    int label = labelTable[y * width + x];
                    
                    if (label != 0)
                    {
                      
                        while (label != labelTable[label])
                            label = labelTable[label - 1];

                        outputRow[x] = (byte)label;
                        
                        
                    }
                }
            }
        }

      
        inputImage.UnlockBits(inputData);
        outputImage.UnlockBits(outputData);
        outputImage.Save("../../../Images/outputLables.bmp");
       

        return outputImage;
    }
    
    
    public static Bitmap SobelFilter(Bitmap inputImage)
    {
        Bitmap filteredImage = new Bitmap(inputImage.Width, inputImage.Height, PixelFormat.Format8bppIndexed);

        BitmapData imageData = inputImage.LockBits(new Rectangle(0, 0, inputImage.Width, inputImage.Height),
            ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);

        BitmapData filteredImageData = filteredImage.LockBits(
            new Rectangle(0, 0, filteredImage.Width, filteredImage.Height),
            ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

        unsafe
        {
            byte * imagePtr = (byte *)imageData.Scan0;
            byte * filteredImagePtr = (byte *)filteredImageData.Scan0;

            int imageStride = imageData.Stride;
            int filteredImageStride = filteredImageData.Stride;

            int width = imageData.Width;
            int height = imageData.Height;

            
            for (int y = 1; y < height - 1; y++)
            {
                for (int x = 1; x < width - 1; x++)
                {

                    int pixelOffset = y * imageStride + x;


                    int gx = imagePtr[pixelOffset - imageStride - 1] - imagePtr[pixelOffset - imageStride + 1]
                        + 2 * imagePtr[pixelOffset - 1] - 2 * imagePtr[pixelOffset + 1]
                        + imagePtr[pixelOffset + imageStride - 1] - imagePtr[pixelOffset + imageStride + 1];

                    int gy = imagePtr[pixelOffset - imageStride - 1] - imagePtr[pixelOffset + imageStride - 1]
                        + 2 * imagePtr[pixelOffset - imageStride] - 2 * imagePtr[pixelOffset + imageStride]
                        + imagePtr[pixelOffset - imageStride + 1] - imagePtr[pixelOffset + imageStride + 1];
                    
                    
                    int gradientMagnitude = (int)Math.Sqrt(gx * gx + gy * gy);

                    
                    byte threshold = 128;
                    byte pixelValue = gradientMagnitude > threshold ? (byte)255 : (byte)0;
                    
                    filteredImagePtr[y * filteredImageStride + x] = pixelValue;
                }
            }
        }

        inputImage.UnlockBits(imageData);
        filteredImage.UnlockBits(filteredImageData);

      

        filteredImage.Save("../../../Images/testImageEnhach.bmp");
        return filteredImage;
    }
    
    
    public static unsafe void Dilate(Bitmap image)
    {

        int[,] kernel = new int[,]
        {
            { 1, 1, 1 },
            { 1, 1, 1 },
            { 1, 1, 1 }
        };

        Bitmap output = new Bitmap(image.Width, image.Height, PixelFormat.Format8bppIndexed);

        BitmapData inputImageData = image.LockBits(new Rectangle(0, 0, image.Width, image.Height),
            ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);
        byte * inputPixelData = (byte *)inputImageData.Scan0.ToPointer();
        int inputStride = inputImageData.Stride;

        BitmapData outputImageData = output.LockBits(new Rectangle(0, 0, output.Width, output.Height),
            ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);
        byte * outputPixelData = (byte *)outputImageData.Scan0.ToPointer();
        int outputStride = outputImageData.Stride;

        unsafe
        {
            int width = image.Width;
            int height = image.Height;

            for (int y = 1; y < height - 1; y++)
            {
                for (int x = 1; x < width - 1; x++)
                {
                    byte max = 0;
                    for (int i = -1; i <= 1; i++)
                    {
                        for (int j = -1; j <= 1; j++)
                        {
                            byte pixel = inputPixelData[(y + i) * inputStride + (x + j)];
                            int k = kernel[i + 1, j + 1];
                            if (k != 0 && pixel > max)
                            {
                                max = pixel;
                            }
                        }
                    }

                    outputPixelData[y * outputStride + x] = max;
                }
            }
        }

        image.UnlockBits(inputImageData);
        output.UnlockBits(outputImageData);

        output.Save("../../../Images/testImageEnhach.bmp");

    }
}